package com.ootpisp.controller.serialization;

import com.ootpisp.model.Employee;

import java.io.*;
import java.util.ArrayList;

public class BinarySerialization implements Serialization {

    @Override
    public void serialize(ArrayList<Employee> employeeList, String fileName) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(employeeList);
            fileOutputStream.close();
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public ArrayList<Employee> deserialize(String fileName) throws Exception {
        ArrayList<Employee> employeeList;
        FileInputStream fileStream = new FileInputStream(fileName);
        ObjectInputStream objectStream = new ObjectInputStream(fileStream);
        employeeList = (ArrayList) objectStream.readObject();
        fileStream.close();
        objectStream.close();
        return employeeList;
    }

}
