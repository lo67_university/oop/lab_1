package com.ootpisp.controller.employee.info;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;

public class ServiceStaffInfo {


    private ComboBox<String> kindOfActivityComboBox;

    public ServiceStaffInfo(ComboBox<String> kindOfActivityComboBox) {
        this.kindOfActivityComboBox = kindOfActivityComboBox;
    }

    public ComboBox<String> getKindOfActivityComboBox() {
        return kindOfActivityComboBox;
    }

}
