package com.ootpisp.model;

public class ServiceStaffFactory implements EmployeeFactory {
    @Override
    public ServiceStaff createEmployee() {
        return new ServiceStaff();
    }
}