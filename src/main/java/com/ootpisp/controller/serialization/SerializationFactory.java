package com.ootpisp.controller.serialization;

public class SerializationFactory {

    public Serialization createSerializer(String extension) {
        switch (extension) {
            case "dat":
                return new BinarySerialization();
            case "json":
                return new JsonSerialization();
            case "txt":
                return new MySerialization();
            default:
                return null;
        }
    }
}
