package com.ootpisp.model;

public class DoctorFactory implements EmployeeFactory {
    @Override
    public Doctor createEmployee() {
        return new Doctor();
    }
}
