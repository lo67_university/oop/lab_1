package com.ootpisp.controller.read;

import com.ootpisp.controller.employee.info.DoctorInfo;
import com.ootpisp.controller.employee.info.MainInfo;
import com.ootpisp.controller.employee.info.MedicalStaffInfo;
import com.ootpisp.model.Doctor;
import com.ootpisp.model.Education;
import com.ootpisp.model.Employee;

public class DoctorReader {
    private MainInfo mainInfo;

    private MedicalStaffInfo medicalStaffInfo;

    private DoctorInfo doctorInfo;

    public DoctorReader(MainInfo mainInfo, MedicalStaffInfo medicalStaffInfo, DoctorInfo doctorInfo) {
        this.mainInfo = mainInfo;
        this.medicalStaffInfo = medicalStaffInfo;
        this.doctorInfo = doctorInfo;
    }

    public void read(Employee employee) {
        Doctor doctor = (Doctor) employee;

        doctor.setProfessionString(Employee.Professions.DOCTOR);

        MedicalStaffReader medicalStaffReader = new MedicalStaffReader(mainInfo, medicalStaffInfo);
        doctor = (Doctor) medicalStaffReader.getMedicalStaffInfo(doctor);

        doctor.setSpecialty(doctorInfo.getSpecialtyTextField().getText());
        doctor.setRoom(doctorInfo.getRoomTextField().getText());
    }
}
