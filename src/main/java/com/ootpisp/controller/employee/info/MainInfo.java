package com.ootpisp.controller.employee.info;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class MainInfo {


    private ComboBox<String> professionComboBox;


    private TextField nameTextField;

    private TextField postTextField;

    private TextField salaryTextField;

    public MainInfo(ComboBox<String> professionComboBox, TextField nameTextField, TextField postTextField, TextField salaryTextField) {
        this.professionComboBox = professionComboBox;
        this.nameTextField = nameTextField;
        this.postTextField = postTextField;
        this.salaryTextField = salaryTextField;
    }

    public ComboBox<String> getProfessionComboBox() {
        return professionComboBox;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getPostTextField() {
        return postTextField;
    }

    public TextField getSalaryTextField() {
        return salaryTextField;
    }

}
