package com.ootpisp.model;

public class NurseFactory implements EmployeeFactory {
    @Override
    public Nurse createEmployee() {
        return new Nurse();
    }
}
