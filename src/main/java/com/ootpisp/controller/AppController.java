package com.ootpisp.controller;

import com.ootpisp.controller.employee.info.*;
import com.ootpisp.controller.read.*;
import com.ootpisp.controller.serialization.*;
import com.ootpisp.controller.write.DoctorWriter;
import com.ootpisp.controller.write.NurseWriter;
import com.ootpisp.controller.write.ServiceStaffWriter;
import com.ootpisp.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.awt.event.ActionEvent;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AppController implements Initializable {

    private ArrayList<Employee> employeesList = new ArrayList<>(30);
    ObservableList<Employee> employeeObservableList;

    private enum Actions {
        ADD,
        EDIT;
    }
    private Actions action;

    private DoctorFactory doctorFactory = new DoctorFactory();
    private NurseFactory nurseFactory = new NurseFactory();
    private ServiceStaffFactory serviceStaffFactory = new ServiceStaffFactory();

    private SerializationFactory serializationFactory = new SerializationFactory();
    //Главная таблица

    @FXML
    private TableView<Employee> employeeTableView;
    @FXML
    private TableColumn<Employee, String> professionColumn;
    @FXML
    private TableColumn<Employee, String> nameColumn;
    @FXML
    private TableColumn<Employee, String> postColumn;
    @FXML
    private TableColumn<Employee, String> salaryColumn;

    //Основные кнопки
    @FXML
    private Button addEmployeeButton;
    @FXML
    private Button editEmployeeButton;
    @FXML
    private Button deleteEmployeeButton;

    @FXML
    private Button serializeButton;
    @FXML
    private Button deserializeButton;

    //Панель со свойствами из таблицы
    @FXML
    private Pane mainPane;

    private final ObservableList<String> professionList =
            FXCollections.observableArrayList("Doctor", "Nurse", "Service Staff");

    @FXML
    private ComboBox<String> professionComboBox;

    @FXML
    private TextField nameTextField;
    @FXML
    private TextField postTextField;
    @FXML
    private TextField salaryTextField;

    private MainInfo mainInfo;

    //Панель со свойствами мед персонала

    @FXML
    private Pane medicalStaffPane;

    @FXML
    private TextField departmentTextField;

    private final ObservableList<String> institutionTypeList =
            FXCollections.observableArrayList("University", "College");
    @FXML
    private ComboBox<String> institutionTypeComboBox;

    @FXML
    private TextField institutionNameTextField;
    @FXML
    private TextField entranceYearTextField;
    @FXML
    private TextField graduationYearTextField;

    private MedicalStaffInfo medicalStaffInfo;

    //Панель со свойствами доктора

    @FXML
    private Pane doctorPane;

    @FXML
    private TextField specialtyTextField;
    @FXML
    private TextField roomTextField;

    private DoctorInfo doctorInfo;

    private DoctorReader doctorReader;

    private DoctorWriter doctorWriter;

    @FXML
    private Button executeDoctorButton;

    //Панель со свойствами медсестры

    @FXML
    private Pane nursePane;

    @FXML
    private CheckBox bioMaterialCheckBox;
    @FXML
    private CheckBox patientCareCheckBox;
    @FXML
    private CheckBox assistanceCheckBox;
    @FXML
    private CheckBox documentationCheckBox;

    private NurseInfo nurseInfo;

    private NurseReader nurseReader;

    private NurseWriter nurseWriter;

    @FXML
    private Button executeNurseButton;

    //Панель со свойствами обслуживающего персонала

    @FXML
    private Pane serviceStaffPane;

    private final ObservableList<String> kindOfActivityList =
            FXCollections.observableArrayList("Supply manager", "Cleaner",
                    "Cook", "An electrician", "Carpenter", "Plumber");

    @FXML
    private ComboBox<String> kindOfActivityComboBox;

    private ServiceStaffInfo serviceStaffInfo;

    private ServiceStaffReader serviceStaffReader;

    private ServiceStaffWriter serviceStaffWriter;

    @FXML
    private Button executeServiceStaffButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        professionComboBox.setItems(professionList);
        institutionTypeComboBox.setItems(institutionTypeList);
        kindOfActivityComboBox.setItems(kindOfActivityList);

        mainInfo = new MainInfo(professionComboBox, nameTextField, postTextField, salaryTextField);
        medicalStaffInfo = new MedicalStaffInfo(departmentTextField, institutionTypeComboBox,
                institutionNameTextField, entranceYearTextField, graduationYearTextField);

        doctorInfo = new DoctorInfo(specialtyTextField, roomTextField);
        doctorReader = new DoctorReader(mainInfo, medicalStaffInfo, doctorInfo);
        doctorWriter = new DoctorWriter(mainInfo, medicalStaffInfo, doctorInfo);

        nurseInfo = new NurseInfo(bioMaterialCheckBox, patientCareCheckBox, assistanceCheckBox, documentationCheckBox);
        nurseReader = new NurseReader(mainInfo, medicalStaffInfo, nurseInfo);
        nurseWriter = new NurseWriter(mainInfo, medicalStaffInfo, nurseInfo);

        serviceStaffInfo = new ServiceStaffInfo(kindOfActivityComboBox);
        serviceStaffReader = new ServiceStaffReader(mainInfo, serviceStaffInfo);
        serviceStaffWriter = new ServiceStaffWriter(mainInfo, serviceStaffInfo);
    }

    @FXML
    private void onAddEmployeeButtonClick() {
        action = Actions.ADD;

        clearAllFields();
        mainPane.setDisable(false);

        executeDoctorButton.setVisible(true);
        executeNurseButton.setVisible(true);
        executeServiceStaffButton.setVisible(true);
        executeDoctorButton.setText("Add");
        executeNurseButton.setText("Add");
        executeServiceStaffButton.setText("Add");
    }

    @FXML
    private void onEditEmployeeButtonClick() {
        action = Actions.EDIT;

        executeDoctorButton.setVisible(true);
        executeNurseButton.setVisible(true);
        executeServiceStaffButton.setVisible(true);
        executeDoctorButton.setText("Edit");
        executeNurseButton.setText("Edit");
        executeServiceStaffButton.setText("Edit");

        drawSelectedEmployee();
    }

    @FXML
    private void onDeleteEmployeeButtonClick() {
        Employee selectedItem = employeeTableView.getSelectionModel().getSelectedItem();
        employeeTableView.getItems().remove(selectedItem);

        if (employeesList.size() == 0) {
            deleteEmployeeButton.setDisable(true);
            editEmployeeButton.setDisable(true);
        }
    }

    private void drawSelectedEmployee() {
        Employee selectedItem = employeeTableView.getSelectionModel().getSelectedItem();
        switch (selectedItem.getProfession()) {
            case "Doctor":
                Doctor doctor = (Doctor) selectedItem;
                doctorWriter.write(doctor);
                break;
            case "Nurse":
                Nurse nurse = (Nurse) selectedItem;
                nurseWriter.write(nurse);
                break;
            case "Service staff":
                ServiceStaff serviceStaff = (ServiceStaff) selectedItem;
                serviceStaffWriter.write(serviceStaff);
                break;
        }
    }

    @FXML
    private void onProfessionComboBoxChange() {
        switch (professionComboBox.getValue()) {
            case "Doctor":
                medicalStaffPane.setVisible(true);
                doctorPane.setVisible(true);

                nursePane.setVisible(false);
                serviceStaffPane.setVisible(false);
                break;
            case "Nurse":
                medicalStaffPane.setVisible(true);
                nursePane.setVisible(true);

                doctorPane.setVisible(false);
                serviceStaffPane.setVisible(false);
                break;
            case "Service Staff":
                serviceStaffPane.setVisible(true);

                medicalStaffPane.setVisible(false);
                doctorPane.setVisible(false);
                nursePane.setVisible(false);
                break;
        }

    }

    @FXML
    private void onExecuteDoctorButtonClick() {
        Doctor newDoctor = doctorFactory.createEmployee();
        executeAction(newDoctor, doctorReader::read);
    }

    @FXML
    private void onExecuteNurseButtonClick() {
        Nurse newNurse = nurseFactory.createEmployee();
        executeAction(newNurse, nurseReader::read);
    }

    @FXML
    private void onExecuteServiceStaffButtonClick() {
        ServiceStaff newServiceStaff = serviceStaffFactory.createEmployee();
        executeAction(newServiceStaff, serviceStaffReader::read);
    }

    private void executeAction(Employee employee, EntityReader<Employee> func) {
        if (isFieldsFilling()) {
            switch (action) {
                case ADD:
                    executeAdd(employee, func);
                    break;
                case EDIT:
                    executeEdit(func);
                    break;
            }
        }
    }

    private void executeAdd(Employee employee, EntityReader<Employee> func) {
        func.read(employee);
        employeesList.add(employee);
        drawEmployeeTable();
        clearAllFields();
    }

    private void executeEdit(EntityReader<Employee> func) {
        int selectedItemNumber = employeeTableView.getSelectionModel().getSelectedIndex();
        Employee selectedItem = employeesList.get(selectedItemNumber);
        func.read(selectedItem);
        drawEmployeeTable();
    }

    private void drawEmployeeTable() {
        employeeObservableList = FXCollections.observableList(employeesList);

        professionColumn.setCellValueFactory(new PropertyValueFactory<>("profession"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        postColumn.setCellValueFactory(new PropertyValueFactory<>("post"));
        salaryColumn.setCellValueFactory(new PropertyValueFactory<>("salaryString"));

        employeeTableView.setItems(employeeObservableList);
        employeeTableView.refresh();
    }

    private void clearAllFields() {
        nameTextField.clear();
        postTextField.clear();
        salaryTextField.clear();
        departmentTextField.clear();
        institutionNameTextField.clear();
        entranceYearTextField.clear();
        graduationYearTextField.clear();
        specialtyTextField.clear();
        roomTextField.clear();
        bioMaterialCheckBox.setSelected(false);
        patientCareCheckBox.setSelected(false);
        assistanceCheckBox.setSelected(false);
        documentationCheckBox.setSelected(false);
    }

    @FXML
    private void onMouseClickedTableView(MouseEvent click) {
        if (click.getClickCount() == 2) {
            drawSelectedEmployee();
            executeDoctorButton.setVisible(false);
            executeNurseButton.setVisible(false);
            executeServiceStaffButton.setVisible(false);
        }
        if (click.getClickCount() == 1) {
            deleteEmployeeButton.setDisable(false);
            editEmployeeButton.setDisable(false);
        }
    }

    private boolean isFieldsFilling() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText("Fields are not filled!");
        if (mainInfo.getProfessionComboBox().getValue() == null) {
            alert.showAndWait();
            return false;
        }

        if (!isMainPaneCorrect()) {
            alert.showAndWait();
            return false;
        }

        if (mainInfo.getProfessionComboBox().getValue().equals("Doctor") || mainInfo.getProfessionComboBox().getValue().equals("Nurse")) {
            if (!isMedicalStaffPaneCorrect()) {
                alert.showAndWait();
                return false;
            }
        }

        if (mainInfo.getProfessionComboBox().getValue().equals("Doctor")) {
            if (!isDoctorPaneCorrect()) {
                alert.showAndWait();
                return false;
            }
        }

        if (mainInfo.getProfessionComboBox().getValue().equals("Service Staff")) {
            if (!isServiceStaffCorrect()) {
                alert.showAndWait();
                return false;
            }
        }
        return true;
    }

    private boolean isMainPaneCorrect() {
        mainInfo.getSalaryTextField().setText(deleteInvalidChar(mainInfo.getSalaryTextField().getText(), false));
        if (mainInfo.getNameTextField().getText().equals("") || mainInfo.getPostTextField().getText().equals("")
                || mainInfo.getSalaryTextField().getText().equals("")) {
            return false;
        }
        return true;
    }

    private boolean isMedicalStaffPaneCorrect() {
        medicalStaffInfo.getEntranceYearTextField().
                setText(deleteInvalidChar(medicalStaffInfo.getEntranceYearTextField().getText(), true));
        medicalStaffInfo.getGraduationYearTextField().
                setText(deleteInvalidChar(medicalStaffInfo.getGraduationYearTextField().getText(), true));
        if (medicalStaffInfo.getDepartmentTextField().getText().equals("") ||
                medicalStaffInfo.getInstitutionTypeComboBox().getValue() == null ||
                medicalStaffInfo.getInstitutionNameTextField().getText().equals("") ||
                medicalStaffInfo.getGraduationYearTextField().getText().equals("")) {
            return false;
        }
        return true;
    }

    private boolean isDoctorPaneCorrect() {
        if (doctorInfo.getSpecialtyTextField().getText().equals("") ||
                (doctorInfo.getRoomTextField()).getText().equals("")) {
            return false;
        }
        return true;
    }

    private boolean isServiceStaffCorrect() {
        if (serviceStaffInfo.getKindOfActivityComboBox().getValue() == null) {
            return false;
        }
        return true;
    }

    private String deleteInvalidChar(String numStr, boolean integerNumber) {
        Set<Character> digits;
        if (integerNumber) {
            digits = Stream.of('0', '1', '2', '3', '4', '5', '6', '7', '8', '9').collect(Collectors.toSet());
        } else {
            digits = Stream.of('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ',').collect(Collectors.toSet());
        }

        StringBuilder str = new StringBuilder(numStr);
        for (int i = 0; i < str.length(); i++) {
            if (!digits.contains(str.charAt(i))) {
                str.deleteCharAt(i);
                i--;
            }
        }
        return str.toString();
    }

    @FXML
    private void onSerializeButtonClick() throws Exception {
        if (!employeesList.isEmpty()) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save Document");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Binary files", "*.dat"),
                    new FileChooser.ExtensionFilter("JSON Documents", "*.json"),
                    new FileChooser.ExtensionFilter("My Serialization files", "*.txt")
            );
            File file = fileChooser.showSaveDialog(nameTextField.getScene().getWindow());
            if (file != null) {
                String fileName = file.getPath();
                int index = fileName.lastIndexOf(".");
                String extension = fileName.substring(index + 1);
                Serialization serialization = serializationFactory.createSerializer(extension);
                serialization.serialize(employeesList, fileName);
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("List is empty!");
            alert.showAndWait();
        }
    }

    @FXML
    private void onDeserializeButtonClick() throws Exception {
        Window window = nameTextField.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Document");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Binary files", "*.dat"),
                new FileChooser.ExtensionFilter("JSON Documents", "*.json"),
                new FileChooser.ExtensionFilter("My Serialization files", "*.txt")
        );
        File file = fileChooser.showOpenDialog(window);
        if (file != null) {
            String fileName = file.getPath();
            int index = fileName.lastIndexOf(".");
            String extension = fileName.substring(index + 1);
            Serialization serialization = serializationFactory.createSerializer(extension);
            employeesList = serialization.deserialize(fileName);
            drawEmployeeTable();
        }
    }


}