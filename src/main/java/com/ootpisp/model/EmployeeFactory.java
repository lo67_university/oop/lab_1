package com.ootpisp.model;

public interface EmployeeFactory {

    Employee createEmployee();

}
