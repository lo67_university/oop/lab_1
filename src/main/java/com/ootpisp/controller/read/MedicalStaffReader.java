package com.ootpisp.controller.read;

import com.ootpisp.controller.employee.info.MainInfo;
import com.ootpisp.controller.employee.info.MedicalStaffInfo;
import com.ootpisp.model.Education;
import com.ootpisp.model.MedicalStaff;

public class MedicalStaffReader{

    MainInfo mainInfo;

    MedicalStaffInfo medicalStaffInfo;

    public MedicalStaffReader(MainInfo mainInfo, MedicalStaffInfo medicalStaffInfo) {
        this.mainInfo = mainInfo;
        this.medicalStaffInfo = medicalStaffInfo;
    }

    public MedicalStaff getMedicalStaffInfo(MedicalStaff employee) {
        employee.setFullName(mainInfo.getNameTextField().getText());
        employee.setPost(mainInfo.getPostTextField().getText());
        employee.setSalary(Float.parseFloat(mainInfo.getSalaryTextField().getText().trim()));

        employee.setDepartment(medicalStaffInfo.getDepartmentTextField().getText());
        Education education = new Education();

        switch (medicalStaffInfo.getInstitutionTypeComboBox().getValue()) {
            case "University":
                education.setInstitutionType(Education.InstitutionTypes.UNIVERSITY);
                break;
            case "College":
                education.setInstitutionType(Education.InstitutionTypes.COLLEGE);
        }

        education.setInstitutionName(medicalStaffInfo.getInstitutionNameTextField().getText());
        education.setEntranceYear(Integer.parseInt(medicalStaffInfo.getEntranceYearTextField().getText()));
        education.setGraduationYear(Integer.parseInt(medicalStaffInfo.getGraduationYearTextField().getText()));

        employee.setEducation(education);

        return employee;
    }

}
