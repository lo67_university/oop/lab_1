package com.ootpisp.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ServiceStaff.class, name = "ServiceStaff"),
})

public abstract class Employee implements Serializable {

    public enum Professions {
        DOCTOR,
        NURSE,
        SERVICE_STAFF
    }

    protected String profession;

    protected String fullName;

    protected String post;

    protected float salary;

    protected String salaryString;

    public Employee() {
    }

    public void setProfessionString(Professions professionEnumElement) {
        switch (professionEnumElement) {
            case DOCTOR:
                profession = "Doctor";
                break;
            case NURSE:
                profession = "Nurse";
                break;
            case SERVICE_STAFF:
                profession = "Service staff";
                break;
        }
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalaryString(String salaryString) {
        this.salaryString = salaryString;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setSalary(float salary) {
        this.salary = salary;
        salaryString = Float.toString(salary);
    }

    public String getProfession() {
        return profession;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPost() {
        return post;
    }

    public String getSalaryString() {
        return salaryString;
    }

    public String writeData(){
        return "";
    }

    public void readData(String[] tokens) {

    }
}
