package com.ootpisp.controller.write;

public interface EntityWriter<Employee> {

    void write(Employee employee);
}
