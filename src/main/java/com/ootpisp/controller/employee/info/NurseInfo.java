package com.ootpisp.controller.employee.info;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;

public class NurseInfo {

    private CheckBox bioMaterialCheckBox;

    private CheckBox patientCareCheckBox;

    private CheckBox assistanceCheckBox;

    private CheckBox documentationCheckBox;

    public NurseInfo(CheckBox bioMaterialCheckBox, CheckBox patientCareCheckBox, CheckBox assistanceCheckBox, CheckBox documentationCheckBox) {
        this.bioMaterialCheckBox = bioMaterialCheckBox;
        this.patientCareCheckBox = patientCareCheckBox;
        this.assistanceCheckBox = assistanceCheckBox;
        this.documentationCheckBox = documentationCheckBox;
    }

    public CheckBox getBioMaterialCheckBox() {
        return bioMaterialCheckBox;
    }

    public CheckBox getPatientCareCheckBox() {
        return patientCareCheckBox;
    }

    public CheckBox getAssistanceCheckBox() {
        return assistanceCheckBox;
    }

    public CheckBox getDocumentationCheckBox() {
        return documentationCheckBox;
    }

}
