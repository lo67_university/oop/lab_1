package com.ootpisp.model;

import java.io.Serializable;

public class Education implements Serializable {

    public enum  InstitutionTypes {
        UNIVERSITY,
        COLLEGE
    }

    public Education() {
    }

    private InstitutionTypes institutionType;

    private String institutionName;

    private int entranceYear;

    private int graduationYear;

    public void setInstitutionType(InstitutionTypes institutionType) {
        this.institutionType = institutionType;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public void setEntranceYear(int entranceYear) {
        this.entranceYear = entranceYear;
    }

    public void setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
    }

    public InstitutionTypes getInstitutionType() {
        return institutionType;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public int getEntranceYear() {
        return entranceYear;
    }

    public int getGraduationYear() {
        return graduationYear;
    }
}
