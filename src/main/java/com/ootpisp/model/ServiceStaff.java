package com.ootpisp.model;

import java.io.Serializable;

public class ServiceStaff extends Employee {

    public enum Activities {
        SUPPLY_MANAGER,
        CLEANER,
        COOK,
        ELECTRICIAN,
        CARPENTER,
        PLUMBER
    }

    public ServiceStaff() {
    }

    private Activities kindOfActivity;

    public void setKindOfActivity(Activities kindOfActivity) {
        this.kindOfActivity = kindOfActivity;
    }

    public Activities getKindOfActivity() {
        return kindOfActivity;
    }

    @Override
    public String writeData() {
        String result = this.getProfession() + ';'
                + this.getFullName() + ';'
                + this.getPost() + ';'
                + this.getSalaryString() + ';'
                + this.getKindOfActivity();
        return result;
    }

    @Override
    public void readData(String[] tokens) {
        this.profession = tokens[0];
        this.fullName = tokens[1];
        this.post = tokens[2];
        this.salaryString = tokens[3];
        this.salary = Float.parseFloat(tokens[3]);
        this.kindOfActivity = Activities.valueOf(tokens[4]);
    }
}
