package com.ootpisp.controller.serialization;

import com.ootpisp.model.Employee;
import javafx.beans.Observable;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public interface Serialization {

    void serialize(ArrayList<Employee> employeeList, String fileName) throws Exception;

    ArrayList<Employee> deserialize(String fileName) throws Exception;

}
