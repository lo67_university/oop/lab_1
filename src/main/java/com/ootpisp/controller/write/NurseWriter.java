package com.ootpisp.controller.write;

import com.ootpisp.controller.employee.info.MainInfo;
import com.ootpisp.controller.employee.info.MedicalStaffInfo;
import com.ootpisp.controller.employee.info.NurseInfo;
import com.ootpisp.model.Education;
import com.ootpisp.model.Nurse;

public class NurseWriter implements EntityWriter<Nurse> {

    private final MainInfo mainInfo;

    private final MedicalStaffInfo medicalStaffInfo;

    private final NurseInfo nurseInfo;

    public NurseWriter(MainInfo mainInfo, MedicalStaffInfo medicalStaffInfo, NurseInfo nurseInfo) {
        this.mainInfo = mainInfo;
        this.medicalStaffInfo = medicalStaffInfo;
        this.nurseInfo = nurseInfo;
    }

    @Override
    public void write(Nurse nurse) {
        mainInfo.getProfessionComboBox().setValue("Nurse");

        mainInfo.getNameTextField().setText(nurse.getFullName());
        mainInfo.getPostTextField().setText(nurse.getPost());
        mainInfo.getSalaryTextField().setText(nurse.getSalaryString());

        medicalStaffInfo.getDepartmentTextField().setText(nurse.getDepartment());

        Education education = nurse.getEducation();
        switch (education.getInstitutionType()) {
            case UNIVERSITY:
                medicalStaffInfo.getInstitutionTypeComboBox().setValue("University");
                break;
            case COLLEGE:
                medicalStaffInfo.getInstitutionTypeComboBox().setValue("College");
        }

        medicalStaffInfo.getInstitutionNameTextField().setText(education.getInstitutionName());
        medicalStaffInfo.getEntranceYearTextField().setText(Integer.toString(education.getEntranceYear()));
        medicalStaffInfo.getGraduationYearTextField().setText(Integer.toString(education.getGraduationYear()));

        for (Nurse.Responsibilities responsibility : nurse.getResponsibilities()) {
            switch (responsibility) {
                case TESTING:
                    nurseInfo.getBioMaterialCheckBox().setSelected(true);
                    break;
                case CARE:
                    nurseInfo.getPatientCareCheckBox().setSelected(true);
                    break;
                case ASSISTANCE:
                    nurseInfo.getAssistanceCheckBox().setSelected(true);
                    break;
                case WORK_WITH_HISTORIES:
                    nurseInfo.getDocumentationCheckBox().setSelected(true);
                    break;
            }
        }
    }

}