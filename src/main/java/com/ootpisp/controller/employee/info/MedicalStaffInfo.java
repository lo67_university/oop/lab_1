package com.ootpisp.controller.employee.info;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class MedicalStaffInfo {


    private TextField departmentTextField;


    private ComboBox<String> institutionTypeComboBox;


    private TextField institutionNameTextField;

    private TextField entranceYearTextField;

    private TextField graduationYearTextField;

    public MedicalStaffInfo(TextField departmentTextField, ComboBox<String> institutionTypeComboBox,
                            TextField institutionNameTextField, TextField entranceYearTextField, TextField graduationYearTextField) {
        this.departmentTextField = departmentTextField;
        this.institutionTypeComboBox = institutionTypeComboBox;
        this.institutionNameTextField = institutionNameTextField;
        this.entranceYearTextField = entranceYearTextField;
        this.graduationYearTextField = graduationYearTextField;
    }

    public TextField getDepartmentTextField() {
        return departmentTextField;
    }

    public ComboBox<String> getInstitutionTypeComboBox() {
        return institutionTypeComboBox;
    }

    public TextField getInstitutionNameTextField() {
        return institutionNameTextField;
    }

    public TextField getEntranceYearTextField() {
        return entranceYearTextField;
    }

    public TextField getGraduationYearTextField() {
        return graduationYearTextField;
    }

}
