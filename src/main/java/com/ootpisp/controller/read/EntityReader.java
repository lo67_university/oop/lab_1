package com.ootpisp.controller.read;

import com.ootpisp.model.Employee;
import com.ootpisp.model.ServiceStaff;

@FunctionalInterface
public interface EntityReader<Employee> {

    void read(Employee employee);

}
