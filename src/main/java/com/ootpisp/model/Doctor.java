package com.ootpisp.model;

import java.io.Serializable;

public class Doctor extends MedicalStaff {

    private String specialty;

    private String room;

    public Doctor() {
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getSpecialty() {
        return specialty;
    }

    public String getRoom() {
        return room;
    }

    @Override
    public String writeData() {
        String result = this.getProfession() + ';'
                + this.getFullName() + ';'
                + this.getPost() + ';'
                + this.getSalaryString() + ';'
                + this.getEducation().getInstitutionType() + ';'
                + this.getEducation().getInstitutionName() + ';'
                + this.getEducation().getEntranceYear() + ';'
                + this.getEducation().getGraduationYear() + ';'
                + this.getDepartment() + ';'
                + this.getSpecialty() + ';'
                + this.getRoom();
        return result;
    }

    @Override
    public void readData(String[] tokens) {
        this.profession = tokens[0];
        this.fullName = tokens[1];
        this.post = tokens[2];
        this.salaryString = tokens[3];
        this.salary = Float.parseFloat(tokens[3]);
        Education education = new Education();
            education.setInstitutionType(Education.InstitutionTypes.valueOf(tokens[4]));
            education.setInstitutionName(tokens[5]);
            education.setEntranceYear(Integer.parseInt(tokens[6]));
            education.setGraduationYear(Integer.parseInt(tokens[7]));
        this.education = education;
        this.department = tokens[8];
        this.specialty = tokens[9];
        this.room = tokens[10];
    }
}
