package com.ootpisp.controller.serialization;

import com.ootpisp.model.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;

public class MySerialization implements Serialization {
    @Override
    public void serialize(ArrayList<Employee> employeeList, String fileName) throws Exception {
        FileWriter writer = new FileWriter(fileName);
        for (Employee employee : employeeList){
            writer.append(employee.writeData());
            writer.append('\n');
        }
        writer.close();
    }

    @Override
    public ArrayList<Employee> deserialize(String fileName) throws Exception {
        FileReader reader = new FileReader(fileName);
        BufferedReader bufferedReader = new BufferedReader(reader);
        ArrayList<Employee> employeeList = new ArrayList<>();
        String line = bufferedReader.readLine();
        while(line != null){
            String[] tokens = line.split(";");
            Employee employee;
            switch (tokens[0]) {
                case "Doctor":{
                    employee = new Doctor();
                    break;
                }
                case "Nurse":{
                    employee = new Nurse();
                    break;
                }
                case "Service staff":{
                    employee = new ServiceStaff();
                    break;
                }
                default:
                   return null;
            }
            employee.readData(tokens);
            employeeList.add(employee);
            line = bufferedReader.readLine();
        }
        reader.close();
        bufferedReader.close();
        return employeeList;
    }
}
