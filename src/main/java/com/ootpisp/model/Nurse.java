package com.ootpisp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Nurse extends MedicalStaff {

    public enum Responsibilities {
        TESTING,
        CARE,
        ASSISTANCE,
        WORK_WITH_HISTORIES
    }

    public Nurse() {
    }

    private List<Responsibilities> responsibilities = new ArrayList<>();

    public void addResponsibility(Responsibilities responsibility) {
        responsibilities.add(responsibility);
    }

    public List<Responsibilities> getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(List<Responsibilities> responsibilities) {
        this.responsibilities = responsibilities;
    }

    @Override
    public String writeData() {
        String result = this.getProfession() + ';'
                + this.getFullName() + ';'
                + this.getPost() + ';'
                + this.getSalaryString() + ';'
                + this.getEducation().getInstitutionType() + ';'
                + this.getEducation().getInstitutionName() + ';'
                + this.getEducation().getEntranceYear() + ';'
                + this.getEducation().getGraduationYear() + ';'
                + this.getDepartment() + ';'
                + this.getResponsibilities();
        return result;
    }

    @Override
    public void readData(String[] tokens) {
        this.profession = tokens[0];
        this.fullName = tokens[1];
        this.post = tokens[2];
        this.salaryString = tokens[3];
        this.salary = Float.parseFloat(tokens[3]);
        Education education = new Education();
            education.setInstitutionType(Education.InstitutionTypes.valueOf(tokens[4]));
            education.setInstitutionName(tokens[5]);
            education.setEntranceYear(Integer.parseInt(tokens[6]));
            education.setGraduationYear(Integer.parseInt(tokens[7]));
        this.education = education;
        this.department = tokens[8];
        String responsibilitiesString = tokens[9].
                replaceAll("^\\[|]$", "").
                replaceAll(" ","");
        ArrayList<String> responsibilitiesList = new ArrayList<>(Arrays.asList(responsibilitiesString.split(",")));
        for (String responsibility : responsibilitiesList) {
            responsibilities.add(Responsibilities.valueOf(responsibility));
        }
    }
}
