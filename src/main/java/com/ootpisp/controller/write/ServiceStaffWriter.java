package com.ootpisp.controller.write;

import com.ootpisp.controller.employee.info.MainInfo;
import com.ootpisp.controller.employee.info.ServiceStaffInfo;
import com.ootpisp.model.ServiceStaff;

public class ServiceStaffWriter implements EntityWriter<ServiceStaff> {

    private final MainInfo mainInfo;

    private final ServiceStaffInfo serviceStaffInfo;

    public ServiceStaffWriter(MainInfo mainInfo, ServiceStaffInfo serviceStaffInfo) {
        this.mainInfo = mainInfo;
        this.serviceStaffInfo = serviceStaffInfo;
    }

    @Override
    public void write(ServiceStaff serviceStaff) {
        mainInfo.getProfessionComboBox().setValue("Service Staff");

        mainInfo.getNameTextField().setText(serviceStaff.getFullName());
        mainInfo.getPostTextField().setText(serviceStaff.getPost());
        mainInfo.getSalaryTextField().setText(serviceStaff.getSalaryString());

        switch (serviceStaff.getKindOfActivity()) {
            case SUPPLY_MANAGER:
                serviceStaffInfo.getKindOfActivityComboBox().setValue("Supply manager");
                break;
            case CLEANER:
                serviceStaffInfo.getKindOfActivityComboBox().setValue("Cleaner");
                break;
            case COOK:
                serviceStaffInfo.getKindOfActivityComboBox().setValue("Cook");
                break;
            case ELECTRICIAN:
                serviceStaffInfo.getKindOfActivityComboBox().setValue("An electrician");
                break;
            case CARPENTER:
                serviceStaffInfo.getKindOfActivityComboBox().setValue("Carpenter");
                break;
            case PLUMBER:
                serviceStaffInfo.getKindOfActivityComboBox().setValue("Plumber");
                break;
        }
    }
}
