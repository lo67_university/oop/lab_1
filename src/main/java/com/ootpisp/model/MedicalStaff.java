package com.ootpisp.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Nurse.class, name = "Nurse"),
        @JsonSubTypes.Type(value = Doctor.class, name = "Doctor")
})

public abstract class MedicalStaff extends Employee {

    protected Education education;

    protected String department;

    public MedicalStaff() {
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Education getEducation() {
        return education;
    }

    public String getDepartment() {
        return department;
    }
}
