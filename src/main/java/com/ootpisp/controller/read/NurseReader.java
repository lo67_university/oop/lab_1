package com.ootpisp.controller.read;

import com.ootpisp.controller.employee.info.MainInfo;
import com.ootpisp.controller.employee.info.MedicalStaffInfo;
import com.ootpisp.controller.employee.info.NurseInfo;
import com.ootpisp.model.Education;
import com.ootpisp.model.Employee;
import com.ootpisp.model.Nurse;

public class NurseReader {

    private MainInfo mainInfo;

    private MedicalStaffInfo medicalStaffInfo;

    private NurseInfo nurseInfo;

    public NurseReader(MainInfo mainInfo, MedicalStaffInfo medicalStaffInfo, NurseInfo nurseInfo) {
        this.mainInfo = mainInfo;
        this.medicalStaffInfo = medicalStaffInfo;
        this.nurseInfo = nurseInfo;
    }

    public void read(Employee employee) {
        Nurse nurse = (Nurse) employee;

        nurse.setProfessionString(Employee.Professions.NURSE);

        MedicalStaffReader medicalStaffReader = new MedicalStaffReader(mainInfo, medicalStaffInfo);
        nurse = (Nurse) medicalStaffReader.getMedicalStaffInfo(nurse);

        if (nurseInfo.getBioMaterialCheckBox().isSelected()) {
            nurse.addResponsibility(Nurse.Responsibilities.TESTING);
        }
        if (nurseInfo.getPatientCareCheckBox().isSelected()) {
            nurse.addResponsibility(Nurse.Responsibilities.CARE);
        }
        if (nurseInfo.getAssistanceCheckBox().isSelected()) {
            nurse.addResponsibility(Nurse.Responsibilities.ASSISTANCE);
        }
        if (nurseInfo.getDocumentationCheckBox().isSelected()) {
            nurse.addResponsibility(Nurse.Responsibilities.WORK_WITH_HISTORIES);
        }
    }
}
