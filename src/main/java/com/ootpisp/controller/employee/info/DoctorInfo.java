package com.ootpisp.controller.employee.info;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class DoctorInfo {


    private TextField specialtyTextField;

    private TextField roomTextField;

    public DoctorInfo(TextField specialtyTextField, TextField roomTextField) {
        this.specialtyTextField = specialtyTextField;
        this.roomTextField = roomTextField;
    }

    public TextField getSpecialtyTextField() {
        return specialtyTextField;
    }

    public TextField getRoomTextField() {
        return roomTextField;
    }

}
