package com.ootpisp.controller.read;

import com.ootpisp.controller.employee.info.MainInfo;
import com.ootpisp.controller.employee.info.ServiceStaffInfo;
import com.ootpisp.model.Employee;
import com.ootpisp.model.ServiceStaff;

public class ServiceStaffReader{

    private MainInfo mainInfo;

    private ServiceStaffInfo serviceStaffInfo;

    public ServiceStaffReader(MainInfo mainInfo, ServiceStaffInfo serviceStaffInfo) {
        this.mainInfo = mainInfo;
        this.serviceStaffInfo = serviceStaffInfo;
    }

    public void read(Employee employee) {
        ServiceStaff serviceStaff = (ServiceStaff) employee;

        serviceStaff.setProfessionString(Employee.Professions.SERVICE_STAFF);
        serviceStaff.setFullName(mainInfo.getNameTextField().getText());
        serviceStaff.setPost(mainInfo.getPostTextField().getText());
        serviceStaff.setSalary(Float.parseFloat(mainInfo.getSalaryTextField().getText().trim()));

        switch (serviceStaffInfo.getKindOfActivityComboBox().getValue()) {
            case "Supply manager":
                serviceStaff.setKindOfActivity(ServiceStaff.Activities.SUPPLY_MANAGER);
                break;
            case "Cleaner":
                serviceStaff.setKindOfActivity(ServiceStaff.Activities.CLEANER);
                break;
            case "Cook":
                serviceStaff.setKindOfActivity(ServiceStaff.Activities.COOK);
                break;
            case "An electrician":
                serviceStaff.setKindOfActivity(ServiceStaff.Activities.ELECTRICIAN);
                break;
            case "Carpenter":
                serviceStaff.setKindOfActivity(ServiceStaff.Activities.CARPENTER);
                break;
            case "Plumber":
                serviceStaff.setKindOfActivity(ServiceStaff.Activities.PLUMBER);
                break;
        }
    }

}
