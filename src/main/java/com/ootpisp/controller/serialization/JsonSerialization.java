package com.ootpisp.controller.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.ootpisp.model.Employee;

import java.io.File;
import java.util.ArrayList;

public class JsonSerialization implements Serialization {
    @Override
    public void serialize(ArrayList<Employee> employeeList, String fileName) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectWrapper objectWrapper = new ObjectWrapper();
        objectWrapper.setAllEditions(employeeList);
        objectMapper.configure( SerializationFeature.INDENT_OUTPUT,true);
        objectMapper.writeValue(new File(fileName),objectWrapper);
    }

    @Override
    public ArrayList<Employee> deserialize(String fileName) throws Exception {
       ObjectMapper objectMapper = new ObjectMapper();
       ObjectWrapper objectWrapper = objectMapper.
               readValue(new File(fileName), ObjectWrapper.class);

        return objectWrapper.getAllEditions();
    }
}
