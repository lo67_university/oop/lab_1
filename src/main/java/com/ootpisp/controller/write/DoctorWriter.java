package com.ootpisp.controller.write;

import com.ootpisp.controller.employee.info.DoctorInfo;
import com.ootpisp.controller.employee.info.MainInfo;
import com.ootpisp.controller.employee.info.MedicalStaffInfo;
import com.ootpisp.model.Doctor;
import com.ootpisp.model.Education;

public class DoctorWriter implements EntityWriter<Doctor> {

    private final MainInfo mainInfo;

    private final MedicalStaffInfo medicalStaffInfo;

    private final DoctorInfo doctorInfo;

    public DoctorWriter(MainInfo mainInfo, MedicalStaffInfo medicalStaffInfo, DoctorInfo doctorInfo) {
        this.mainInfo = mainInfo;
        this.medicalStaffInfo = medicalStaffInfo;
        this.doctorInfo = doctorInfo;
    }

    @Override
    public void write(Doctor doctor) {
        mainInfo.getProfessionComboBox().setValue("Doctor");

        mainInfo.getNameTextField().setText(doctor.getFullName());
        mainInfo.getPostTextField().setText(doctor.getPost());
        mainInfo.getSalaryTextField().setText(doctor.getSalaryString());

        medicalStaffInfo.getDepartmentTextField().setText(doctor.getDepartment());

        Education education = doctor.getEducation();
        switch (education.getInstitutionType()) {
            case UNIVERSITY:
                medicalStaffInfo.getInstitutionTypeComboBox().setValue("University");
                break;
            case COLLEGE:
                medicalStaffInfo.getInstitutionTypeComboBox().setValue("College");
        }

        medicalStaffInfo.getInstitutionNameTextField().setText(education.getInstitutionName());
        medicalStaffInfo.getEntranceYearTextField().setText(Integer.toString(education.getEntranceYear()));
        medicalStaffInfo.getGraduationYearTextField().setText(Integer.toString(education.getGraduationYear()));

        doctorInfo.getSpecialtyTextField().setText(doctor.getSpecialty());
        doctorInfo.getRoomTextField().setText(doctor.getRoom());
    }

}
