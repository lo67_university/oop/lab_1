package com.ootpisp.controller.serialization;

import com.ootpisp.model.Employee;

import java.util.ArrayList;

public class ObjectWrapper {

    private ArrayList<Employee> allEditions;

    public ArrayList<Employee> getAllEditions() {
        return allEditions;
    }

    public void setAllEditions(ArrayList<Employee> allEditions) {
        this.allEditions = allEditions;
    }
}
